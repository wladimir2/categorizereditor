/**
* ����� ��� ��������� ���������� ������������� ������������
*
*/

//------------------------------------------------------------------------------

#include "ErrorService/CommonException.h"
#include "Resource/CE_CategorizerEditorStrings.h"

#include "CategorizerEditor/CE_TType.h"

//------------------------------------------------------------------------------

namespace CategorizerEditor
{

    //--------------------------------------------------------------------------

    /**
    * ����������� �� ��������� ���������
    */
    TType::TType( void )
    {
        m_VirtualType.add( Common::vtInt,       INTEGER_TYPE );
        m_VirtualType.add( Common::vtString,    STRING_TYPE );
        m_VirtualType.add( Common::vtDate,      DATE_TYPE );
        m_VirtualType.add( Common::vtDouble,    DOUBLE_TYPE );
        m_VirtualType.add( Common::vtBlob,      BLOB_TYPE );
        m_VirtualType.add( Common::vtBoolean,   BOOLEAN_TYPE );
        m_VirtualType.add( Common::vtOneToOne,  ONETOONE_TYPE );
        m_VirtualType.add( Common::vtOneToMany, ONETOMANY_TYPE );
    }

    /**
    * �����������
    * @return ������ �� �����
    */
    TType& TType::getInstance( void )
    {
        static TType type;
        return type;
    }

    /**
    * ����������
    */
    TType::~TType( void )
    {

    }

    //--------------------------------------------------------------------------

    /**
    * �������� ��������� ������������� ���� ����
    * @param _Type ��� ����
    * @return ��������� ������������� ���� ����
    */
    std::string TType::getVirtualTypeText( Common::TVirtualType _Type ) const
    {
        return m_VirtualType.toString( _Type );
    }

    /**
    * �������� ��� ���� �� ��� ���������� �������������
    * @param _Text ��������� ������������� ���� ����
    * @return ��� ����
    */
    Common::TVirtualType TType::getVirtualType( const std::string& _Text ) const
    {
        return m_VirtualType.fromString( _Text );
    }

    //--------------------------------------------------------------------------

} // namespace CategorizerEditor

//------------------------------------------------------------------------------
