/**
* ��������� ��� �������� ������������ �������� ���� ������ ��� ��������
*
*/

#ifndef CE_IValueControllerH
    #define CE_IValueControllerH

    //--------------------------------------------------------------------------

    #include <boost/shared_ptr.hpp>

    //--------------------------------------------------------------------------

    #include "Common/Data/C_IFieldDescription.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        class IValueController
        {
            protected:
                /**
                * ��������� �� ��������
                */
                Common::TCFieldDescriptionPtr m_pDescription;

                /**
                * ����������� ����������� �� ���������
                */
                IValueController( const IValueController& );
                /**
                * �������� ������������ �� ���������
                */
                IValueController& operator = ( const IValueController& );
            public:
                /**
                * �����������
                * @param _pDescription ��������� �� ��������
                */
                IValueController( const Common::TCFieldDescriptionPtr& _pDescription );
                /**
                * ����������
                */
                virtual ~IValueController( void );

                //--------------------------------------------------------------

                /**
                * �������� ��������� �� ��������
                * @return ��� ��������
                */
                Common::TCFieldDescriptionPtr getDescription( void ) const
                {
                    return m_pDescription;
                }

                /**
                * ��������� �������� �� ������������ ��������
                * @param _Value ��������
                */
                virtual void check( const std::string& _Value ) const = 0;

                //--------------------------------------------------------------

        }; // class IValueController

        //----------------------------------------------------------------------

        typedef boost::shared_ptr < IValueController > TValueControllerPtr;

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // CE_IValueControllerH
