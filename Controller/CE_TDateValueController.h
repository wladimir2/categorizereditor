/**
* ����� ��� �������� ������������ �������� DATE ���� ������ ��� ��������
*
*/

#ifndef CE_TDateValueControllerH
    #define CE_TDateValueControllerH

    //--------------------------------------------------------------------------

    #include "CategorizerEditor/Controller/CE_IValueController.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        class TDateValueController : public IValueController
        {
            private:
                /**
                * ����������� ����������� �� ���������
                */
                TDateValueController( const TDateValueController& );
                /**
                * �������� ������������ �� ���������
                */
                TDateValueController& operator = ( const TDateValueController& );
            public:
                /**
                * �����������
                * @param _pDescription ��������� �� ��������
                */
                TDateValueController( const Common::TCFieldDescriptionPtr& _pDescription );
                /**
                * ����������
                */
                ~TDateValueController( void );

                //--------------------------------------------------------------

                /**
                * ��������� �������� �� ������������ ��������
                * @param _Value ��������
                */
                void check( const std::string& _Value ) const;

                //--------------------------------------------------------------

        }; // class TBoolValueController

        //----------------------------------------------------------------------

        typedef boost::shared_ptr < TDateValueController > TDateValueControllerPtr;

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // CE_TDateValueControllerH
