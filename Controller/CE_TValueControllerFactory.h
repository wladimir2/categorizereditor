/**
* ����� ( ��������� ����� ) �������� ����������� ������������ ( �������� ������������ �������� ���� ������ ��� �������� )
*
*/

#ifndef CE_TValueControllerFactoryH
    #define CE_TValueControllerFactory

    //--------------------------------------------------------------------------

    #include <map>

    //--------------------------------------------------------------------------

    #include "CategorizerEditor/Controller/CE_IValueController.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        class TValueControllerFactory
        {
            private:
                /**
                * ����������� ��������� ������� ������������
                */
                struct TAbstractCreator
                {
                    virtual TValueControllerPtr create( const Common::TCFieldDescriptionPtr& _pDescription ) = 0;
                };
                /**
                * ���������� ��������� ������� �����������
                */
                template< class T > struct TControllerCreator : TAbstractCreator
                {
                    virtual TValueControllerPtr create( const Common::TCFieldDescriptionPtr& _pDescription )
                    {
                        return TValueControllerPtr( new T( _pDescription ) );
                    };
                };

                //--------------------------------------------------------------

                typedef boost::shared_ptr < TAbstractCreator > TAbstractCreatorPtr;
                typedef std::map< Common::TVirtualType, TAbstractCreatorPtr > TMap;

                //--------------------------------------------------------------

                /**
                * ��������� �������, ��������� ���������� �����������
                */
                TMap m_Container;

                /**
                * ����������� ����������� �� ���������
                */
                TValueControllerFactory( const TValueControllerFactory& );
                /**
                * �������� ������������ �� ���������
                */
                TValueControllerFactory& operator = ( const TValueControllerFactory& );
            public:
                /**
                * �����������
                */
                TValueControllerFactory( void );

                //--------------------------------------------------------------

                /**
                * ������� ��������� ������ ����������� � ������� ��������� �� ���� �� ��� ��������������
                * @param _pDescription ��������� �� ��������
                * @return ��������� �� ��������� ������ �����������
                */
                TValueControllerPtr create( const Common::TCFieldDescriptionPtr& _pDescription ) const;

                //--------------------------------------------------------------

        }; // class TValueControllerFactory

        //----------------------------------------------------------------------

        typedef boost::shared_ptr < TValueControllerFactory > TValueControllerFactoryPtr;

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // CE_TValueControllerFactory
