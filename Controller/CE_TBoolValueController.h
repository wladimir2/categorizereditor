/**
* ����� ��� �������� ������������ �������� BOOL ���� ������ ��� ��������
*
*/

#ifndef CE_TBoolValueControllerH
    #define CE_TBoolValueControllerH

    //--------------------------------------------------------------------------

    #include "CategorizerEditor/Controller/CE_IValueController.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        class TBoolValueController : public IValueController
        {
            private:
                /**
                * ����������� ����������� �� ���������
                */
                TBoolValueController( const TBoolValueController& );
                /**
                * �������� ������������ �� ���������
                */
                TBoolValueController& operator = ( const TBoolValueController& );
            public:
                /**
                * �����������
                * @param _pDescription ��������� �� ��������
                */
                TBoolValueController( const Common::TCFieldDescriptionPtr& _pDescription );
                /**
                * ����������
                */
                ~TBoolValueController( void );

                //--------------------------------------------------------------

                /**
                * ��������� �������� �� ������������ ��������
                * @param _Value ��������
                */
                void check( const std::string& _Value ) const;

                //--------------------------------------------------------------

        }; // class TBoolValueController

        //----------------------------------------------------------------------

        typedef boost::shared_ptr < TBoolValueController > TBoolValueControllerPtr;

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // CE_TBoolValueControllerH
