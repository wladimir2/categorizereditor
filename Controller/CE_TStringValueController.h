/**
* ����� ��� �������� ������������ �������� STRING ���� ������ ��� ��������
*
*/

#ifndef CE_TStringValueControllerH
    #define CE_TStringValueControllerH

    //--------------------------------------------------------------------------

    #include "CategorizerEditor/Controller/CE_IValueController.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        class TStringValueController : public IValueController
        {
            private:
                /**
                * ����������� ����������� �� ���������
                */
                TStringValueController( const TStringValueController& );
                /**
                * �������� ������������ �� ���������
                */
                TStringValueController& operator = ( const TStringValueController& );
            public:
                /**
                * �����������
                * @param _pDescription ��������� �� ��������
                */
                TStringValueController( const Common::TCFieldDescriptionPtr& _pDescription );
                /**
                * ����������
                */
                ~TStringValueController( void );

                //--------------------------------------------------------------

                /**
                * ��������� �������� �� ������������ ��������
                * @param _Value ��������
                */
                void check( const std::string& _Value ) const;

                //--------------------------------------------------------------

        }; // class TIntValueController

        //----------------------------------------------------------------------

        typedef boost::shared_ptr < TStringValueController > TStringValueControllerPtr;

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // CE_TStringValueControllerH
