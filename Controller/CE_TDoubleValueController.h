/**
* ����� ��� �������� ������������ �������� DOUBLE ���� ������ ��� ��������
*
*/

#ifndef CE_TDoubleValueControllerH
    #define CE_TDoubleValueControllerH

    //--------------------------------------------------------------------------

    #include "CategorizerEditor/Controller/CE_IValueController.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        class TDoubleValueController : public IValueController
        {
            private:
                /**
                * ����������� ����������� �� ���������
                */
                TDoubleValueController( const TDoubleValueController& );
                /**
                * �������� ������������ �� ���������
                */
                TDoubleValueController& operator = ( const TDoubleValueController& );
            public:
                /**
                * �����������
                * @param _pDescription ��������� �� ��������
                */
                TDoubleValueController( const Common::TCFieldDescriptionPtr& _pDescription );
                /**
                * ����������
                */
                ~TDoubleValueController( void );

                //--------------------------------------------------------------

                /**
                * ��������� �������� �� ������������ ��������
                * @param _Value ��������
                */
                void check( const std::string& _Value ) const;

                //--------------------------------------------------------------

        }; // class TDoubleValueController

        //----------------------------------------------------------------------

        typedef boost::shared_ptr < TDoubleValueController > TDoubleValueControllerPtr;

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // CE_TDoubleValueControllerH
