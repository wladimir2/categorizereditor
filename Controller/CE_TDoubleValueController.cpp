/**
* ����� ��� �������� ������������ �������� DOUBLE ���� ������ ��� ��������
*
*/

//------------------------------------------------------------------------------

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

//------------------------------------------------------------------------------

#include "ErrorService/SmartException.h"
#include "Resource/CE_CategorizerEditorStrings.h"

#include "CategorizerEditor/Controller/CE_TDoubleValueController.h"

//------------------------------------------------------------------------------

namespace CategorizerEditor
{

    //--------------------------------------------------------------------------

    /**
    * �����������
    * @param _pDescription ��������� �� ��������
    */
    TDoubleValueController::TDoubleValueController( const Common::TCFieldDescriptionPtr& _pDescription ) : IValueController( _pDescription )
    {

    }

    /**
    * ����������
    */
    TDoubleValueController::~TDoubleValueController( void )
    {

    }

    //--------------------------------------------------------------------------

    /**
    * ��������� �������� �� ������������ ��������
    * @param _Value ��������
    */
    void TDoubleValueController::check( const std::string& _Value ) const
    {
        // �������� �� �������������� ����������
        if ( ( m_pDescription->isRequired() == true ) && ( _Value.empty() == true ) )
        {
            throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_FILLINGREQUIRED ) % m_pDescription->getUserName() ).str() );
        }
        // �������� �� ������������ ����
        if ( ( m_pDescription->isRequired() == true ) || ( ( m_pDescription->isRequired() == false ) && ( _Value.empty() == false ) ) )
        {
            try
            {
                boost::lexical_cast< double >( _Value );
            }
            catch( ... )
            {
                throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_TYPE ) % m_pDescription->getUserName() ).str() );
            }
        }
    }

    //--------------------------------------------------------------------------

} // namespace CategorizerEditor

//------------------------------------------------------------------------------
