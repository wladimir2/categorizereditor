/**
* ����� ��� �������� ������������ �������� STRING ���� ������ ��� ��������
*
*/

//------------------------------------------------------------------------------

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

//------------------------------------------------------------------------------

#include "ErrorService/SmartException.h"
#include "Resource/CE_CategorizerEditorStrings.h"

#include "CategorizerEditor/Controller/CE_TStringValueController.h"

//------------------------------------------------------------------------------

namespace CategorizerEditor
{

    //--------------------------------------------------------------------------

    /**
    * �����������
    * @param _pDescription ��������� �� ��������
    */
    TStringValueController::TStringValueController( const Common::TCFieldDescriptionPtr& _pDescription ) : IValueController( _pDescription )
    {

    }

    /**
    * ����������
    */
    TStringValueController::~TStringValueController( void )
    {

    }

    //--------------------------------------------------------------------------

    /**
    * ��������� �������� �� ������������ ��������
    * @param _Value ��������
    */
    void TStringValueController::check( const std::string& _Value ) const
    {
        // �������� �� �������������� ����������
        if ( ( m_pDescription->isRequired() == true ) && ( _Value.empty() == true ) )
        {
            throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_FILLINGREQUIRED ) % m_pDescription->getUserName() ).str() );
        }
        // �������� �� ������������ ������
        if ( m_pDescription->getSize() < _Value.size() )
        {
            throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_SIZE ) % m_pDescription->getUserName() % boost::lexical_cast< std::string >( m_pDescription->getSize() ) ).str() );
        }
    }

    //--------------------------------------------------------------------------

} // namespace CategorizerEditor

//------------------------------------------------------------------------------
