/**
* ����� ��� �������� ������������ �������� BLOB ���� ������ ��� ��������
*
*/

//------------------------------------------------------------------------------

#include <qobject.h>
#include <qfileinfo.h>

//------------------------------------------------------------------------------

#include "ErrorService/SmartException.h"
#include "Resource/CE_CategorizerEditorStrings.h"
#include "Common/MacroHelpers.h"

#include "CategorizerEditor/Controller/CE_TBlobValueController.h"

//------------------------------------------------------------------------------

namespace CategorizerEditor
{

    //--------------------------------------------------------------------------

    /**
    * �����������
    * @param _pDescription ��������� �� ��������
    */
    TBlobValueController::TBlobValueController( const Common::TCFieldDescriptionPtr& _pDescription ) : IValueController( _pDescription )
    {

    }

    /**
    * ����������
    */
    TBlobValueController::~TBlobValueController( void )
    {

    }

    //--------------------------------------------------------------------------

    /**
    * ��������� �������� �� ������������ ��������
    * @param _Value ��������
    */
    void TBlobValueController::check( const std::string& /*_Value*/ ) const
    {

    }

    //--------------------------------------------------------------------------

} // namespace CategorizerEditor

//------------------------------------------------------------------------------
