/**
* ����� ��� �������� ������������ �������� REFERENCE ONE_TO_ONE ���� ������ ��� ��������
*
*/

//------------------------------------------------------------------------------

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

//------------------------------------------------------------------------------

#include "ErrorService/SmartException.h"
#include "Resource/CE_CategorizerEditorStrings.h"

#include "Common/Data/C_IDataFactory.h"

#include "CategorizerEditor/Controller/CE_TReferenceValueController.h"

//------------------------------------------------------------------------------

namespace CategorizerEditor
{

    //--------------------------------------------------------------------------

    /**
    * �����������
    * @param _pDescription ��������� �� ��������
    */
    TReferenceValueController::TReferenceValueController( const Common::TCFieldDescriptionPtr& _pDescription ) : IValueController( _pDescription )
    {

    }

    /**
    * ����������
    */
    TReferenceValueController::~TReferenceValueController( void )
    {

    }

    //--------------------------------------------------------------------------

    /**
    * ��������� �������� �� ������������ ��������
    * @param _Value ��������
    */
    void TReferenceValueController::check( const std::string& _Value ) const
    {
        // �������� �� �������������� ����������
        if ( ( m_pDescription->isRequired() == true ) && ( _Value.empty() == true ) )
        {
            throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_FILLINGREQUIRED ) % m_pDescription->getUserName() ).str() );
        }
        // �������� �� ������������ ����
        int ID;
        if ( ( m_pDescription->isRequired() == true ) || ( ( m_pDescription->isRequired() == false ) && ( _Value.empty() == false ) ) )
        {
            try
            {
                ID = boost::lexical_cast< int >( _Value );
            }
            catch( ... )
            {
                throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_TYPE ) % m_pDescription->getUserName() ).str() );
            }
        }
        // �������� �� ������������� ������ � ��������� �������
        try
        {
            if ( _Value.empty() == false )
            {
                Common::TDataEditorPtr pReferenceData = m_pDescription->getFactory()->createDataEditor( m_pDescription->getReferenceName() );
                Common::TRecord Record = pReferenceData->findRecord( ID );
                if ( Record.Values.size() == 0 )
                {
                    throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_BY_ID ) % m_pDescription->getUserName() ).str() );
                }
            }
        }
        catch ( ... )
        {
            throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_BY_ID ) % m_pDescription->getUserName() ).str() );
        }
    }

    //--------------------------------------------------------------------------

} // namespace CategorizerEditor

//------------------------------------------------------------------------------
