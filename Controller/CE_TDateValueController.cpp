/**
* ����� ��� �������� ������������ �������� DATE ���� ������ ��� ��������
*
*/

//------------------------------------------------------------------------------

#include <boost/format.hpp>

//------------------------------------------------------------------------------

#include "Common/DateTime/DateTime.h"
#include "ErrorService/SmartException.h"
#include "Resource/CE_CategorizerEditorStrings.h"

#include "CategorizerEditor/Controller/CE_TDateValueController.h"

//------------------------------------------------------------------------------

namespace CategorizerEditor
{

    //--------------------------------------------------------------------------

    /**
    * �����������
    * @param _pDescription ��������� �� ��������
    */
    TDateValueController::TDateValueController( const Common::TCFieldDescriptionPtr& _pDescription ) : IValueController( _pDescription )
    {

    }

    /**
    * ����������
    */
    TDateValueController::~TDateValueController( void )
    {

    }

    //--------------------------------------------------------------------------

    /**
    * ��������� �������� �� ������������ ��������
    * @param _Value ��������
    */
    void TDateValueController::check( const std::string& _Value ) const
    {
        // �������� �� �������������� ����������
        if ( ( m_pDescription->isRequired() == true ) && ( _Value.empty() == true ) )
        {
            throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_FILLINGREQUIRED ) % m_pDescription->getUserName() ).str() );
        }
        // �������� �� ������������ ����
        if ( ( m_pDescription->isRequired() == true ) || ( ( m_pDescription->isRequired() == false ) && ( _Value.empty() == false ) ) )
        {
            try
            {
                Common::BDStringToDate( _Value );
            }
            catch( ... )
            {
                throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_TYPE ) % m_pDescription->getUserName() ).str() );
            }
        }
    }

    //--------------------------------------------------------------------------

} // namespace CategorizerEditor

//------------------------------------------------------------------------------
