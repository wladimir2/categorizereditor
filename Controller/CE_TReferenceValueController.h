/**
* ����� ��� �������� ������������ �������� REFERENCE ONE_TO_ONE ���� ������ ��� ��������
*
*/

#ifndef CE_TReferenceValueControllerH
    #define CE_TReferenceValueControllerH

    //--------------------------------------------------------------------------

    #include "CategorizerEditor/Controller/CE_IValueController.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        class TReferenceValueController : public IValueController
        {
            private:
                /**
                * ����������� ����������� �� ���������
                */
                TReferenceValueController( const TReferenceValueController& );
                /**
                * �������� ������������ �� ���������
                */
                TReferenceValueController& operator = ( const TReferenceValueController& );
            public:
                /**
                * �����������
                * @param _pDescription ��������� �� ��������
                */
                TReferenceValueController( const Common::TCFieldDescriptionPtr& _pDescription );
                /**
                * ����������
                */
                ~TReferenceValueController( void );

                //--------------------------------------------------------------

                /**
                * ��������� �������� �� ������������ ��������
                * @param _Value ��������
                */
                void check( const std::string& _Value ) const;

                //--------------------------------------------------------------

        }; // class TReferenceValueController

        //----------------------------------------------------------------------

        typedef boost::shared_ptr < TReferenceValueController > TReferenceValueControllerPtr;

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // CE_TReferenceValueControllerH
