/**
* ����� ( ��������� ����� ) �������� ����������� ������������ ( �������� ������������ �������� ���� ������ ��� �������� )
*
*/

//------------------------------------------------------------------------------

#include "ErrorService/SmartException.h"
#include "Resource/CE_CategorizerEditorStrings.h"

#include "CategorizerEditor/Controller/CE_TBlobValueController.h"
#include "CategorizerEditor/Controller/CE_TBoolValueController.h"
#include "CategorizerEditor/Controller/CE_TDateValueController.h"
#include "CategorizerEditor/Controller/CE_TIntValueController.h"
#include "CategorizerEditor/Controller/CE_TDoubleValueController.h"
#include "CategorizerEditor/Controller/CE_TStringValueController.h"

#include "CategorizerEditor/Controller/CE_TReferenceValueController.h"

#include "CategorizerEditor/Controller/CE_TValueControllerFactory.h"

//------------------------------------------------------------------------------

namespace CategorizerEditor
{

    //--------------------------------------------------------------------------

    /**
    * �����������
    */
    TValueControllerFactory::TValueControllerFactory( void )
    {
        m_Container[ Common::vtBlob ] = TAbstractCreatorPtr( new TControllerCreator< TBlobValueController > );
        m_Container[ Common::vtBoolean ] = TAbstractCreatorPtr( new TControllerCreator< TBoolValueController > );
        m_Container[ Common::vtDate ] = TAbstractCreatorPtr( new TControllerCreator< TDateValueController > );
        m_Container[ Common::vtInt ] = TAbstractCreatorPtr( new TControllerCreator< TIntValueController > );
        m_Container[ Common::vtDouble ] = TAbstractCreatorPtr( new TControllerCreator< TDoubleValueController > );
        m_Container[ Common::vtString ] = TAbstractCreatorPtr( new TControllerCreator< TStringValueController > );

        m_Container[ Common::vtReference ] = TAbstractCreatorPtr( new TControllerCreator< TReferenceValueController > );
        m_Container[ Common::vtOneToOne ] = TAbstractCreatorPtr( new TControllerCreator< TReferenceValueController > );
        m_Container[ Common::vtOneToMany ] = TAbstractCreatorPtr( new TControllerCreator< TReferenceValueController > );
    }

    //--------------------------------------------------------------------------

    /**
    * ������� ��������� ������ ����������� � ������� ��������� �� ���� �� ��� ��������������
    * @param _pDescription ��������� �� ��������
    * @return ��������� �� ��������� ������ �����������
    */
    TValueControllerPtr TValueControllerFactory::create( const Common::TCFieldDescriptionPtr& _pDescription ) const
    {
        // �������� �� ������������ ��������� �� ��������
        NULL_POINTER( _pDescription.get(), ErrorService::ECommonException )

        TMap::const_iterator IT = m_Container.find( _pDescription->getType() );
        // �������� �� ������������ ��������������
        if ( IT == m_Container.end() )
        {
            throw ErrorService::ECommonException( ERROR_VALUE_TYPE );
        }
        return IT->second->create( _pDescription );
    }

    //--------------------------------------------------------------------------

} // namespace CategorizerEditor

//------------------------------------------------------------------------------
