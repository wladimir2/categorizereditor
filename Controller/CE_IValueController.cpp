/**
* ��������� ��� �������� ������������ �������� ���� ������ ��� ��������
*
*/

//------------------------------------------------------------------------------

#include "ErrorService/SmartException.h"

#include "CategorizerEditor/Controller/CE_IValueController.h"

//------------------------------------------------------------------------------

namespace CategorizerEditor
{

    //--------------------------------------------------------------------------

    /**
    * �����������
    * @param _pDescription ��������� �� ��������
    */
    IValueController::IValueController( const Common::TCFieldDescriptionPtr& _pDescription )
    {
        // �������� �� ������������ ��������� �� ��������
        NULL_POINTER( _pDescription.get(), ErrorService::ECommonException )

        m_pDescription = _pDescription;
    }

    /**
    * ����������
    */
    IValueController::~IValueController( void )
    {

    }

    //--------------------------------------------------------------------------

} // namespace CategorizerEditor

//------------------------------------------------------------------------------
