/**
* ����� ��� �������� ������������ �������� INT ���� ������ ��� ��������
*
*/

#ifndef CE_TIntValueControllerH
    #define CE_TIntValueControllerH

    //--------------------------------------------------------------------------

    #include "CategorizerEditor/Controller/CE_IValueController.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        class TIntValueController : public IValueController
        {
            private:
                /**
                * ����������� ����������� �� ���������
                */
                TIntValueController( const TIntValueController& );
                /**
                * �������� ������������ �� ���������
                */
                TIntValueController& operator = ( const TIntValueController& );
            public:
                /**
                * �����������
                * @param _pDescription ��������� �� ��������
                */
                TIntValueController( const Common::TCFieldDescriptionPtr& _pDescription );
                /**
                * ����������
                */
                ~TIntValueController( void );

                //--------------------------------------------------------------

                /**
                * ��������� �������� �� ������������ ��������
                * @param _Value ��������
                */
                void check( const std::string& _Value ) const;

                //--------------------------------------------------------------

        }; // class TIntValueController

        //----------------------------------------------------------------------

        typedef boost::shared_ptr < TIntValueController > TIntValueControllerPtr;

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // CE_TIntValueControllerH
