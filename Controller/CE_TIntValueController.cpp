/**
* ����� ��� �������� ������������ �������� INT ���� ������ ��� ��������
*
*/

//------------------------------------------------------------------------------

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

//------------------------------------------------------------------------------

#include "ErrorService/SmartException.h"
#include "Resource/CE_CategorizerEditorStrings.h"

#include "CategorizerEditor/Controller/CE_TIntValueController.h"

//------------------------------------------------------------------------------

namespace CategorizerEditor
{

    //--------------------------------------------------------------------------

    /**
    * �����������
    * @param _pDescription ��������� �� ��������
    */
    TIntValueController::TIntValueController( const Common::TCFieldDescriptionPtr& _pDescription ) : IValueController( _pDescription )
    {

    }

    /**
    * ����������
    */
    TIntValueController::~TIntValueController( void )
    {

    }

    //--------------------------------------------------------------------------

    /**
    * ��������� �������� �� ������������ ��������
    * @param _Value ��������
    */
    void TIntValueController::check( const std::string& _Value ) const
    {
        // �������� �� �������������� ����������
        if ( ( m_pDescription->isRequired() == true ) && ( _Value.empty() == true ) )
        {
            throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_FILLINGREQUIRED ) % m_pDescription->getUserName() ).str() );
        }
        // �������� �� ������������ ����
        if ( ( m_pDescription->isRequired() == true ) || ( ( m_pDescription->isRequired() == false ) && ( _Value.empty() == false ) ) )
        {
            try
            {
                boost::lexical_cast< int >( _Value );
            }
            catch( ... )
            {
                throw ErrorService::ECommonException( ( boost::format( ERROR_VALUE_TYPE ) % m_pDescription->getUserName() ).str() );
            }
        }
    }

    //--------------------------------------------------------------------------

} // namespace CategorizerEditor

//------------------------------------------------------------------------------
