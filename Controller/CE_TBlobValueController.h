/**
* ����� ��� �������� ������������ �������� BLOB ���� ������ ��� ��������
*
*/

#ifndef CE_TBlobValueControllerH
    #define CE_TBlobValueControllerH

    //--------------------------------------------------------------------------

    #include "CategorizerEditor/Controller/CE_IValueController.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        class TBlobValueController : public IValueController
        {
            private:
                /**
                * ����������� ����������� �� ���������
                */
                TBlobValueController( const TBlobValueController& );
                /**
                * �������� ������������ �� ���������
                */
                TBlobValueController& operator = ( const TBlobValueController& );
            public:
                /**
                * �����������
                * @param _pDescription ��������� �� ��������
                */
                TBlobValueController( const Common::TCFieldDescriptionPtr& _pDescription );
                /**
                * ����������
                */
                ~TBlobValueController( void );

                //--------------------------------------------------------------

                /**
                * ��������� �������� �� ������������ ��������
                * @param _Value ��������
                */
                void check( const std::string& _Value ) const;

                //--------------------------------------------------------------

        }; // class TBlobValueController

        //----------------------------------------------------------------------

        typedef boost::shared_ptr < TBlobValueController > TBlobValueControllerPtr;

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // CE_TBlobValueControllerH
