/**
* ����� ��� ��������� ���������� ������������� ������������
*
*/

#ifndef CE_TTypeH
    #define CE_TTypeH

    //--------------------------------------------------------------------------

    #include <string>

    //--------------------------------------------------------------------------

    #include "Common/Utils/EnumPrinter.h"
    #include "Common/VirtualType.h"

    //--------------------------------------------------------------------------

    namespace CategorizerEditor
    {

        //----------------------------------------------------------------------

        #define TEXT_TYPE_CE ( CategorizerEditor::TType::getInstance() )

        //----------------------------------------------------------------------

        class TType
        {
            private:
                /**
                * ��� �����
                */
                Common::TEnumPrinter< Common::TVirtualType > m_VirtualType;

                /**
                * ����������� �� ��������� ���������
                */
                TType( void );
                /**
                * ����������� ����������� �� ��������� ���������
                */
                TType( const TType& );
                /**
                * �������� ������������ �� ��������� ���������
                */
                TType& operator = ( const TType& );
            public:
                /**
                * �����������
                * @return ������ �� �����
                */
                static TType& getInstance( void );
                /**
                * ����������
                */
                ~TType( void );

                //--------------------------------------------------------------

                /**
                * �������� ��������� ������������� ���� ����
                * @param _Type ��� ����
                * @return ��������� ������������� ���� ����
                */
                std::string getVirtualTypeText( Common::TVirtualType _Type ) const;
                /**
                * �������� ��� ���� �� ��� ���������� �������������
                * @param _Text ��������� ������������� ���� ����
                * @return ��� ����
                */
                Common::TVirtualType getVirtualType( const std::string& _Text ) const;

                //--------------------------------------------------------------

        }; // class TType

        //----------------------------------------------------------------------

    } // namespace CategorizerEditor

    //--------------------------------------------------------------------------

#endif // define CE_TTypeH
